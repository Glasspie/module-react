
import styles from './baskets.module.css'
import CardBasket from '../../components/elements/cardBasket/cardBasket.js';
import { removeCardBasket } from '../../store/reducers/products';
import { authLogout } from '../../store/reducers/auth';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

function Baskets() {
    const basket = useSelector(state => state.products.basket)
    const allPrice = useSelector(state => state.products.allPriceProductsBasket)

    const dispatch = useDispatch()
    const handleLogout = () => {
        dispatch(authLogout());
    };

    return (
        <div className={styles.baskets}>
            <header className={styles.baskets__header}>
                <div className={styles.header__left}>
                    <Link to='/products'><button className={styles.baskets__buttonArrow}></button></Link>
                    <h1 className={styles.title}>Корзина с выбранными товарами</h1>
                </div>
                <div className={styles.header__right}>
                    <button className="concrete-card__buttons-item btn-exit" onClick={handleLogout}>Выйти</button>
                </div>

            </header>
            <main className={styles.main}>
                {basket.map(item => {
                    return (
                        <CardBasket
                            key={item.id}
                            url={item.url}
                            title={item.title}
                            price={item.price}
                            remove={() => dispatch(removeCardBasket(item.idx))}
                            />
                    )
                })}
            </main>
            <div className={styles.wrapper}>
            <div className={styles.line}></div>
            <footer className={styles.footer}>
                <p className={styles.footer__title}>Заказ на сумму:<span className={styles.footer__sum}>{allPrice} ₽</span></p>
                <button className={styles.footer__buttonConfirm}>Оформить заказ</button>
            </footer>
            </div>

        </div>
    )
}

export default Baskets;
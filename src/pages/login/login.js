import styles from './login.module.css'
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { authLogin } from "../../store/reducers/auth";
import { Link } from 'react-router-dom';

export const Login = () => {
    const dispatch = useDispatch();

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/products`;
        navigate(path);
    };

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const handleClick = () => {
        dispatch(authLogin({ login: login, password: password }));
        routeChange();
    };


    return (
        <div className={styles.container}>
            <div className={styles.authorization}>
                <p className={styles.authorization__nav}>
                <Link to="/" className={styles.authorization__link}>Зарегистрироваться</Link>
                </p>
                <h1 className={styles.authorization__title}>Вход</h1>
                <form className={styles.authorization__form} action="/products">
                    <input
                        id="login"
                        type="text"
                        placeholder="Логин"
                        className={styles.input}
                        value={login}
                        onChange={(e) => setLogin(e.target.value)}
                    ></input>
                    <label for id="login" className={styles.authorization__error}>
                        Поле не должно быть пустым
                    </label>

                    <input
                    id="pass"
                        type="password"
                        placeholder="Пароль"
                        className={styles.input}
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    ></input>
                    <label for id="pass" className={styles.authorization__error}>
                        Поле не должно быть пустым
                    </label>

                    {/* <div className={styles.checkbox}>
                    <input
                    id="checkbox__input"
                    className={styles.checkbox__input}
                    type="checkbox"
                    ></input>

                    <label for id="checkbox__input" className={styles.checkbox__label}>
                        Я согласен получать обновления на почту
                    </label>
                    </div> */}

                    <button className={styles.button} onClick={handleClick}>
                    Войти
                    </button>
                </form>
            </div>
        </div>
    );
};

import styles from './registration.module.css'
import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { authRegistration } from '../../store/reducers/auth';
import { Link } from 'react-router-dom';

function Registration() {
    let logins = JSON.parse(localStorage.getItem("logins"));
    if (logins === null) {
        logins = [];
    }

    const dispatch = useDispatch();
    
    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/login`;
        navigate(path);
    };

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const handleClick = () => {
        let flag = false;
        logins.forEach((item) => {
            if (item.login === login) {
            item.password = password;
            flag = true;
            }
        });
        if (!flag) {
            logins.push({
            login: login,
            password: password,
            });
        }
        localStorage.setItem("logins", JSON.stringify(logins));
    
        dispatch(authRegistration({ login: login, password: password }));
        routeChange();
    };
    
    return (
        <div className={styles.container}>
            <div className={styles.authorization}>
                <p className={styles.authorization__nav}>
                <Link to="/login" className={styles.authorization__link}>Авторизоваться</Link>
                </p>
                <h1 className={styles.authorization__title}>Регистрация</h1>
                <form className={styles.authorization__form} action="/products">
                    <input
                        id="login"
                        type="text"
                        placeholder="Логин"
                        className={styles.input}
                        value={login}
                        onChange={(e) => setLogin(e.target.value)}
                    ></input>
                    <label for id="login" className={styles.authorization__error}>
                        Поле не должно быть пустым
                    </label>

                    <input
                    id="pass"
                        type="password"
                        placeholder="Пароль"
                        className={styles.input}
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    ></input>
                    <label for id="pass" className={styles.authorization__error}>
                        Поле не должно быть пустым
                    </label>

                    {/* <div className={styles.checkbox}>
                    <input
                    id="checkbox__input"
                    className={styles.checkbox__input}
                    type="checkbox"
                    ></input>

                    <label for id="checkbox__input" className={styles.checkbox__label}>
                        Я согласен получать обновления на почту
                    </label>
                    </div> */}

                    <button className={styles.button} onClick={handleClick}>
                    Зарегистрироваться
                    </button>
                </form>
            </div>
            
        </div>
    )
}

export default Registration;
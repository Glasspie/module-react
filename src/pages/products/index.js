// import './products.css'
import styles from './products.module.css'
import Card from '../../components/elements/card/card.js';
import { authLogout } from '../../store/reducers/auth';
import products from '../../products.js';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';

import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

function Products() {

    const count = useSelector(state => state.products.countProductsInBasket)
    const allPrice = useSelector(state => state.products.allPriceProductsBasket)

    const dispatch = useDispatch();

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/basket`;
        navigate(path);
    };
    
    const cardChange = (id) => {
    console.log("cardChange id=", id);
    let path = `/products/${id}`;
    navigate(path);
    };

    const handleLogout = () => {
        dispatch(authLogout());
    };

    return (
        <div className={styles.products}>
            <div className={styles.products__header}>
                <h1 className={styles.title}>наша продукция</h1>
                <div className={styles.products__calc}>
                    <div className={styles.products__list}>
                        <p>{count} товара</p>
                        <p>на сумму {allPrice} ₽</p>
                    </div>
                    <Link to='/basket'><button className={styles.products__button}></button></Link>
                    <button className="concrete-card__buttons-item btn-exit" onClick={handleLogout}>Выйти</button>
                </div>
            </div>
            <main className={styles.main}>
                {products.map(item => {
                    return(
                        <Card
                            key={item.id}
                            id={item.id}
                            url={item.url}
                            title={item.title}
                            description={item.description}
                            price={item.price}
                            quantity={item.quantity}
                            
                        />
                    )
                })}
            </main>
        </div>
    );
}

export default Products;
import { createSlice } from "@reduxjs/toolkit";

// import {products} from '../../products'
import { defer } from "react-router-dom";

const initialState = {
    // products: products,
    basket: [],
    countProductsInBasket:0,
    allPriceProductsBasket:0,
}

export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        addBasket: (state, payload) => {
            state.basket.push(payload.payload)

            state.countProductsInBasket = state.basket.length

            state.allPriceProductsBasket = state.basket.reduce((sum, current) => {
                return sum + current.price
            }, 0)
        },
        
        removeCardBasket: (state, payload) => {
            state.basket = state.basket.filter((item) => {
                return item.idx !== payload.payload
            })

            state.countProductsInBasket = state.basket.length

            state.allPriceProductsBasket = state.basket.reduce((sum, current) => {
                return sum + current.price
            }, 0)
        }
    }
})

export const {addBasket, removeCardBasket} = productsSlice.actions

export default productsSlice.reducer
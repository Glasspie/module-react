import styles from './card.module.css'
import { useSelector, useDispatch } from 'react-redux';
import { addBasket } from '../../../store/reducers/products';
import { v4 as uuidv4 } from 'uuid';
import { useNavigate } from 'react-router-dom';

function Card({id, url, title, description, price, quantity}) {
    const dispatch = useDispatch()

    let navigate = useNavigate();
    const routeChange = () => {
        let path = `/basket`;
        navigate(path);
    };
    
    const cardChange = (id) => {
    console.log("cardChange id=", id);
    let path = `/products/${id}`;
    navigate(path);
    };

    const add = () => {
        const item = {
            id: id,
            idx: uuidv4(),
            url: url,
            title: title,
            price: price,
        }

        dispatch(addBasket(item))
    }

    return (
        <div className={styles.card}>

            <div className={styles.main} onClick={() => cardChange(id)}>
                <img className={styles.card__preview} src={url} alt={title}/>
                <h3 className={styles.card__title}>{title}</h3>
                <p className={styles.card__description}>{description}</p>
            </div>

            <div className={styles.card__footer}>
                <p className={styles.card__price}>{price} ₽ <span className={styles.card__quantity}>{quantity}</span></p>
                <button className={styles.card__button} onClick={add}></button>
            </div>
        </div>
    );
}

export default Card;
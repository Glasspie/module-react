import styles from './cardBasket.module.css'

function CardBasket({url, title, price, remove}) {
    return (
        <div className={styles.card}>
            <div className={styles.card__half}>
                <img className={styles.card__preview} src={url} alt="" />
                <h3 className={styles.card__title}>{title}</h3>
            </div>
            <div className={styles.card__half}>
                <p className={styles.card__price}>{price} ₽</p>
                <button onClick={remove} className={styles.card__button}></button>
            </div>
        </div>
    )
}

export default CardBasket;
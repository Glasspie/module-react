import "./card.css";
import { useParams } from "react-router-dom";
import products from "../../../products";
import { useNavigate } from "react-router-dom";
import { authLogout } from "../../../store/reducers/auth";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

function ConcreteCard() {
    const params = useParams();
    const prodId = params.id;

    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(authLogout());
    };

    const count = useSelector(state => state.products.countProductsInBasket)
    const allPrice = useSelector(state => state.products.allPriceProductsBasket)

    return (
        <div className="concrete-card__wrapper">

            <div className="card">
                <div className="concrete-card__header">
                    <Link to='/products'><button className="baskets__buttonArrow"></button></Link>
                    <div className="header__group">
                        <div className="products__calc">
                            <div className="products__list">
                                <p>{count} товара</p>
                                <p>на сумму {allPrice} ₽</p>
                            </div>
                        </div>
                        <Link to='/basket'><button className="products__button"></button></Link>
                        <button className="concrete-card__buttons-item btn-exit" onClick={handleLogout}>Выйти</button>
                    </div>
                </div>

                <div className="concrete-card__main">
                    <div className="preview__wrapper">
                        <img className="card__preview" src={products[+prodId].url} alt=""></img>
                    </div>

                    <div className="description__wrapper">
                        <div className="description__box">
                            <h2 className="card__title">{products[+prodId].title}</h2>
                            <p className="card__description">{products[+prodId].description}{products[+prodId].description}{products[+prodId].description}{products[+prodId].description}{products[+prodId].description}{products[+prodId].description}{products[+prodId].description}</p>
                        </div>
                        <div className="concrete-card__footer">
                            <div className="order__items">
                                <p className="card__price">{products[+prodId].price} ₽</p>
                                <span className="item_value"></span>
                                <p className="item_weigth">{products[+prodId].quantity}</p>
                            </div>

                            <div className="concrete-card__buttons">
                            <Link to='/basket'><button
                                className="concrete-card__buttons-item btn-basket"
                                >
                                В корзину
                                </button></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ConcreteCard;

import { Route } from "react-router-dom";
import './reset.css'
import Products from './pages/products/index.js'
import Baskets from './pages/basket/Baskets.jsx'
import Registration from "./pages/registration/registration";
import { Login } from "./pages/login/login";
import { Routes } from "react-router-dom";
import ConcreteCard from "./components/elements/concreteCard";
import { useSelector } from "react-redux";

function App() {
  const isAuth = useSelector((state) => state.auth.isAuth);
  return (
    <div className="App">
      <Routes>
        <Route
          path="/products/:id"
          element={isAuth ? <ConcreteCard /> : <Registration />}
        ></Route>

        <Route
          path="/basket"
          element={<Baskets />}
        ></Route>

        <Route
          path="/products"
          element={isAuth ? <Products /> : <Registration />}
        ></Route>

        <Route path="/login" element={<Login />}></Route>

        <Route path="/" element={<Registration />}></Route>
      </Routes>
    </div>
  );
}

export default App;
